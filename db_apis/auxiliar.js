const oracledb = require('oracledb');
const database = require('../services/database.js');

const baseQuery =
    `SELECT codcia "codcia",
codaux "codaux",
 nomaux "nomaux",
  procedencia "procedencia",
   clfaux "clfaux",
    codiden "codiden",
     nroiden "nroiden",
      ruc "ruc",
       diraux "diraux",
        codpais "codpais",
         cod_ubigeo "cod_ubigeo",
          localidad "localidad",
           contactos "contactos",
            telfnos "telfnos",
             faxaux "faxaux",
              e_mail "e_mail",
               clacli "clacli",
                clapro "clapro",
                 claser "claser",
                  clatra "clatra",
                   claacc "claacc",
                    claesp "claesp",
                     claotr "claotr",
                      clatrans "clatrans",
                       flgsit "flgsit" FROM GN_AUXI`;

async function find() {

    let query = baseQuery;
    //const binds = {};

    /*if (context.id) {
        binds.employee_id = context.id;

        query += `\nwhere employee_id = :employee_id`;
    }*/

    const result = await database.simpleExecute(query);

    return result.rows;
}

module.exports.find = find;

/*const createSql =
    `insert into employees (
    first_name,
    last_name,
    email,
    phone_number,
    hire_date,
    job_id,
    salary,
    commission_pct,
    manager_id,
    department_id
  ) values (
    :first_name,
    :last_name,
    :email,
    :phone_number,
    :hire_date,
    :job_id,
    :salary,
    :commission_pct,
    :manager_id,
    :department_id
  ) returning employee_id
  into :employee_id`;

async function create(emp) {
    const employee = Object.assign({}, emp);

    employee.employee_id = {
        dir: oracledb.BIND_OUT,
        type: oracledb.NUMBER
    }

    const result = await database.simpleExecute(createSql, employee);

    employee.employee_id = result.outBinds.employee_id[0];

    return employee;
}

module.exports.create = create;

const updateSql =
    `update employees
  set first_name = :first_name,
    last_name = :last_name,
    email = :email,
    phone_number = :phone_number,
    hire_date = :hire_date,
    job_id = :job_id,
    salary = :salary,
    commission_pct = :commission_pct,
    manager_id = :manager_id,
    department_id = :department_id
  where employee_id = :employee_id`;

async function update(emp) {
    const employee = Object.assign({}, emp);
    const result = await database.simpleExecute(updateSql, employee);

    if (result.rowsAffected && result.rowsAffected === 1) {
        return employee;
    } else {
        return null;
    }
}

module.exports.update = update;

const deleteSql =
    `begin

    delete from job_history
    where employee_id = :employee_id;

    delete from employees
    where employee_id = :employee_id;

    :rowcount := sql%rowcount;

  end;`

async function del(id) {
    const binds = {
        employee_id: id,
        rowcount: {
            dir: oracledb.BIND_OUT,
            type: oracledb.NUMBER
        }
    }
    const result = await database.simpleExecute(deleteSql, binds);

    return result.outBinds.rowcount === 1;
}

module.exports.delete = del;
*/