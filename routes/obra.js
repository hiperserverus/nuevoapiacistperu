const express = require('express');
const app = new express.Router();
const obra = require('../controllers/obra.js');

app.route('/obra')
    .get(obra.obtenerObras);
//.post(employees.post)
//.put(employees.put)
//.delete(employees.delete);

module.exports = app;