const express = require('express');
const app = new express.Router();
const vendedor = require('../controllers/vendedor.js');

app.route('/vendedor')
    .get(vendedor.obtenerVendedores)
    //.post(employees.post)
    //.put(employees.put)
    //.delete(employees.delete);

module.exports = app;