const express = require('express');
const app = new express.Router();
const usuario = require('../controllers/usuario.js');

app.route('/usuario')
    .get(usuario.obtenerUsuarios);

module.exports = app;