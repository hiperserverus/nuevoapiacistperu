const usuarios = require('../db_apis/usuario.js');

async function obtenerUsuarios(req, res, next) {
    try {

        const rows = await usuarios.find();

        if (req.params.id) {
            if (rows.length === 1) {
                res.status(200).json(rows[0]);
            } else {
                res.status(404).end();
            }
        } else {
            res.status(200).json(rows);
        }
    } catch (err) {
        next(err);
    }
}

module.exports.obtenerUsuarios = obtenerUsuarios;