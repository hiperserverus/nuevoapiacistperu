const oracledb = require('oracledb');
const database = require('../services/database.js');

const baseQuery =
    `SELECT codcia "codcia",
     codven "codven", 
     nomven "nomven", 
     user_id "user_id", 
     flgest "flgest" FROM GN_VEN`;

async function find() {

    let query = baseQuery;

    const result = await database.simpleExecute(query);

    return result.rows;
}

module.exports.find = find;