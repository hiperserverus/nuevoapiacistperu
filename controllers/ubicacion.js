const ubicaciones = require('../db_apis/ubicacion.js');

async function obtenerUbicaciones(req, res, next) {
    try {

        const rows = await ubicaciones.find();

        if (req.params.id) {
            if (rows.length === 1) {
                res.status(200).json(rows[0]);
            } else {
                res.status(404).end();
            }
        } else {
            res.status(200).json(rows);
        }
    } catch (err) {
        next(err);
    }
}

module.exports.obtenerUbicaciones = obtenerUbicaciones;