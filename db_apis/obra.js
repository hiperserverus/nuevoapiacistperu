const oracledb = require('oracledb');
const database = require('../services/database.js');

const baseQuery =
    `SELECT codcia "codcia",
     codaux "codaux", 
     codobr "codbr",
      desobr "desobr",
       codzon "codzon", 
       dirobr "dirobr",
        contacto "contacto",
         fchini "fchini", 
         fchfin "fchfin",
          codven "codven",
           codgrp "codgrp",
            flgest "flgest" FROM GN_AUXOBR`;

async function find() {

    let query = baseQuery;

    const result = await database.simpleExecute(query);

    return result.rows;
}

module.exports.find = find;