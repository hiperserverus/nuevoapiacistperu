const express = require('express');
const app = new express.Router();
const auxiliar = require('../controllers/auxiliar.js');

app.route('/auxiliar')
    .get(auxiliar.obtenerAuxiliares)
    //.post(employees.post)
    //.put(employees.put)
    //.delete(employees.delete);

module.exports = app;