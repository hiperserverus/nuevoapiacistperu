const oracledb = require('oracledb');
const database = require('../services/database.js');

const baseQuery =
    `SELECT codpais "codpais",
     cod_ubigeo "cod_ubigeo",
      departamento "departamento",
       provincia "provincia", 
       distrito "distrito", 
       cod_postal "cod_postal" FROM GN_UBIGEOS`;

async function find() {

    let query = baseQuery;

    const result = await database.simpleExecute(query);

    return result.rows;
}

module.exports.find = find;