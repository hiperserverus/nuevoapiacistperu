const oracledb = require('oracledb');
const database = require('../services/database.js');

const baseQuery =
    `SELECT codcia "codcia",
     user_id "user_id",
      nom_user "nom_user", 
      email "email",
       flgest "flgest" FROM PF_USERCIA`;

async function find() {

    let query = baseQuery;

    const result = await database.simpleExecute(query);

    return result.rows;
}

module.exports.find = find;