const express = require('express');
const app = new express.Router();
const ubicacion = require('../controllers/ubicacion.js');

app.route('/ubicacion')
    .get(ubicacion.obtenerUbicaciones)
    //.post(employees.post)
    //.put(employees.put)
    //.delete(employees.delete);

module.exports = app;